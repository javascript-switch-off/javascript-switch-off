// This file is part of JavaScript switch off
//
// Copyright (C) 2010, 2011, 2012 Ivaylo Valkov <ivaylo@e-valkov.org>
//
// JavaScript switch off is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// JavaScript switch off is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with JavaScript switch off.  If not, see <http://www.gnu.org/licenses/>.
//
// JavaScript switch off version svn-trunk
// Turn off (or on) JavaScript from the right-click popup menu (and the
// statusbar when available) in Epiphany.
//
// Home page: http://e-valkov.org/epiphany-extensions/js-switch-off/

Epiphany = imports.gi.Epiphany;
Gdk = imports.gi.Gdk;
GConf = imports.gi.GConf;
GLib = imports.gi.GLib;
Gio  = imports.gi.Gio;

// Epiphany versions above 3.x have no statusbar. Used to detect
// different versions of Epiphany and add the appropriate interface.
var ephy_no_statusbar = true;

// Epiphany 2.x uses GConf for settings. Version 3.x uses GSettings/dconf
var ephy_cfg_backend = new Object();

ephy_cfg_backend["gconf"] = new Object();
ephy_cfg_backend["gconf"].gconf_client =  null;
ephy_cfg_backend["gconf"].gconf_notify_id = null
ephy_cfg_backend["gconf"].key =
    "/apps/epiphany/web/javascript_enabled";

ephy_cfg_backend["gsettings"] = new Object();
ephy_cfg_backend["gsettings"].schema = "org.gnome.Epiphany.web";
ephy_cfg_backend["gsettings"].key = "enable-javascript";
ephy_cfg_backend["gsettings"].instance = null;

ephy_cfg_backend.active = "gsettings";

var statusbar_evbox_key = "EphyJsSwitchOffExtensionStatusbarEvbox";
var document_popup_entry_key = "EphyJsSwitchOffExtensionDocumentPopupEntry";

var js_icon_on = GLib.build_filenamev(["/usr", "share",
				       "epiphany-extensions",
				       "js-switch-off",
				       "js-switch-on.png"]);

if (!GLib.file_test(js_icon_on, GLib.FileTest.EXISTS))
{
    js_icon_on = GLib.build_filenamev([GLib.get_home_dir(),
				       ".gnome2", "epiphany",
				       "extensions","data", 
				       "js-switch-off", 
				       "js-switch-on.png"]);
}

var js_icon_off = GLib.build_filenamev(["/usr", "share", 
					"epiphany-extensions",
					"js-switch-off", 
					"js-switch-off.png"]);

if (!GLib.file_test(js_icon_off, GLib.FileTest.EXISTS))
{
    js_icon_off = GLib.build_filenamev([GLib.get_home_dir(), ".gnome2",
					"epiphany", "extensions","data",
					"js-switch-off",
					"js-switch-off.png"]);
}

// Keep track of windows. Used to change all the statusbar icons.
// Event-driven replacement of the icon on GConf value change with
// every event_box, did not work for some reason. All callbacks
// received only one of the window/event_box/icon.
var windows = new Array();

// Returns an icon representing the JavaScript state (on/off)
// according to the is_js_on argument(0/1; true/false)
var create_js_state_icon = function(is_js_on)
{
    var icon_path = get_icon_path_for_js_state(is_js_on);
    var icon = new Gtk.Image.from_file(icon_path);

    return icon;
}

// Returns the Gio.Icon (GIcon) that is rendered on the popup menu.
var create_js_state_gicon = function(is_js_on)
{
    var icon_path = get_icon_path_for_js_state(is_js_on);
    var gfile = Gio.file_new_for_path(icon_path);
    var gicon = null;
    try
    {
	// Crashes in Parabola: c_new is undefined
	gicon = new Gio.FileIcon.c_new(gfile);
    }
    catch(e)
    {
	// Does not create an icon in some distributions, just a black
	// square. :(
	gicon = new Gio.FileIcon({file: gfile});
    }

    return gicon;
}

// Returns the path of the icon depending on the state
// (enabled/disabled) of the JavaScript engine, passed in is_js_on
// variable.
var get_icon_path_for_js_state = function(is_js_on)
{
    var icon_path = null;

    if (is_js_on)
    {
	icon_path = js_icon_on;
    }
    else
    {
	icon_path = js_icon_off;
    }

    return icon_path; 
}

// Returns the text that shows the JavaScript engine state. Used as a
// tooltip or a menu item.
var create_js_state_text = function(is_js_on)
{
    var text = null;

    if (is_js_on)
    {
	text = "Turn off JavaScript";
    }
    else
    {
	text = "Turn on JavaScript";
    }

    return text;
}

// Set the tooltip of the event box that holds the icon on the
// statusbar according to the is_js_on argument.
var set_event_box_tooltip = function(event_box, is_js_on)
{
    var text = create_js_state_text(is_js_on);
    event_box.set_tooltip_text(text);

    return event_box; 
}

// Called before/when the extension is enabled.  Makes the statusbar
// icon and attaches it.
var create_statusbar_icon = function(window)
{
    var statusbar = window.get_statusbar();
    
    if (!statusbar)
    {
	return null;
    }

    event_box = new Gtk.EventBox();

    var gconf_client = ephy_cfg_backend.gconf.gconf_client;
    var is_js_on =  get_js_state();

    var icon = create_js_state_icon(is_js_on);
    set_event_box_tooltip(event_box ,is_js_on);

    event_box.add(icon);
    event_box.show();
    icon.show();

    statusbar.add_widget(event_box);

    statusbar.set_data(statusbar_evbox_key, event_box);
    event_box.signal.
	button_press_event.connect(statusbar_icon_clicked_cb);

    statusbar[statusbar_evbox_key] = event_box;
}

// Called before the extension is disabled.  Removes the statusbar
// icon.
var destroy_statusbar_icon = function(window)
{
    var statusbar = window.get_statusbar();

    if (!statusbar)
    {
	return null;
    }

    var event_box = statusbar[statusbar_evbox_key];

    statusbar.remove_widget(event_box);
}

// Called when the GConf key for enabled/disabled JavaScript had
// changed.
var gconf_value_changed_cb  = function(gconf_client, cncx_id, entry)
{
    var is_js_on = entry.value.get_bool();

    for (var w=0, l=windows.length; w<l; w++)
    {
	var window = windows[w];

	var popup_data = window[document_popup_entry_key];

	if (popup_data)
	{
	    var text = "_"+create_js_state_text(is_js_on);
	    popup_data.action.label = text;

	    var gicon = create_js_state_gicon(is_js_on);
	    popup_data.action.set_gicon(gicon);
	}

	var statusbar = window.get_statusbar();
	
	var event_box = statusbar[statusbar_evbox_key];
	var icon = event_box.get_child();
    
	if (icon)
	{
	    icon.destroy();
	}
	
	icon = create_js_state_icon(is_js_on);
	set_event_box_tooltip(event_box,is_js_on);
	
	event_box.add(icon);
	event_box.show();
	icon.show();
    }
}

// The statusbar icon is clicked.
var statusbar_icon_clicked_cb = function(event_box, event)
{
    var gconf_client = ephy_cfg_backend.gconf.gconf_client;
    var is_js_on = get_js_state();

    // Change the setting to opposite value
    set_js_state(!is_js_on);

    return true;
}

// Creates the entry in the menu shown on right mouse button click
// in the borwser.
var create_ephy_document_popup_entry = function(window)
{
    var manager = window.get_ui_manager();
    var data = new Object();
    var is_js_on = get_js_state();
    var text = "_"+create_js_state_text(is_js_on);

    var action = new Gtk.Action({
	name: "JSSwitchOff",
	// Add and accelerator for the first letter of the text:
	// _Turn...
	label: text,
	tooltip: "Turn on/off the JavaScript engine for all browser windows.",
	stock_id: null
    });

   action.signal.activate.connect(right_click_menu_action_activated_cb);

    var action_group = new Gtk.ActionGroup({
	name: "JSSwitchOffActionGroup"
    });

    var gicon = create_js_state_gicon(is_js_on);
    action.set_gicon(gicon);

    action_group.add_action(action);

    manager.insert_action_group(action_group);

    data.action = action;
    data.action_group = action_group;
    data.ui_id = manager.new_merge_id();

    manager.add_ui(data.ui_id, "/EphyDocumentPopup", "JSSwitchOffSep",
		   null, Gtk.UIManagerItemType.SEPARATOR, false);

    manager.add_ui(data.ui_id, "/EphyDocumentPopup", "JSSwitchOff",
		   "JSSwitchOff", Gtk.UIManagerItemType.MENUITEM, false);

    window[document_popup_entry_key] = data;
}

// Destroys the entry in the menu shown on right mouse button click
// in the borwser.
var destroy_ephy_document_popup_entry = function(window)
{
    var manager = window.get_ui_manager();
    var data =  window[document_popup_entry_key];

    if (!data)
    {
	return null;
    }

    manager.remove_ui(data.ui_id);
    manager.remove_action_group(data.action_group);
    delete window[document_popup_entry_key];
}

// Called when the GSettings key for enabled/disabled JavaScript had changed.
var gsettings_value_changed_cb = function(gsettings_client, key)
{
    var key = ephy_cfg_backend.gsettings.key;
    var is_js_on = gsettings_client.get_boolean(key);
    var text = "_"+create_js_state_text(is_js_on);

    for (var w=0, l=windows.length; w<l; w++)
    {
	var window = windows[w];
	var data = window[document_popup_entry_key];

	if (!data)
	{
	    continue;
	}

	data.action.label = text;

	var gicon = create_js_state_gicon(is_js_on);
	data.action.set_gicon(gicon);
    }
}

// The menu item is activated/clicked
var right_click_menu_action_activated_cb = function(gtk_action)
{
    var is_js_on = get_js_state();

    // Change the setting to opposite value
    set_js_state(!is_js_on);

    return true;
}

// Check if Epiphany has a statusbar, so an icon can be rendered
// there.
var detect_statusbar_capability = function(window)
{
    // Epiphany verions after 3.x do not hava a statusbar
    if (typeof(window.get_statusbar) == "function")
    {
	ephy_no_statusbar = false;
    }
    else
    {
	ephy_no_statusbar = true;
    }
}

// Find out how Epiphany stores settings GCOnf vs. Gsettings.
var detect_cfg_backend = function()
{
    try
    {
	var schemas = Gio.Settings.list_schemas();
	var backend = "gsettings"
	var schema_installed = false;

	if (typeof(schemas.length) == "undefined")
	{
	    backend = "gconf";
	}
	else 
	{
	    for (var i=0, l=schemas.length;i<l; i++)
	    {
		if (schemas[i] == ephy_cfg_backend.gsettings.schema)
		{
		    schema_installed = true;
		    break;
		}
	    }
	}
    }
    catch(e)
    {
	backend = "gconf";
    }

    if (schema_installed)
    {
	backend = "gsettings";
    }
    else
    {
	backend = "gconf";
    }

    ephy_cfg_backend.active = backend;
}

// Get the state (enabled/disabled) of the JavaScript engine by using
// the active configuration backend (GConf/GSettings).
var get_js_state = function()
{
    var is_js_on = true;

    if (ephy_cfg_backend.active == "gsettings")
    {
	// Gsettings
	var gsettings_client = ephy_cfg_backend.gsettings.instance;

	is_js_on =
	    gsettings_client.get_boolean(ephy_cfg_backend.gsettings.key);
    }
    else
    {
	// GCOnf
	var gconf_client = ephy_cfg_backend.gconf.gconf_client;
	is_js_on = gconf_client.get_bool(ephy_cfg_backend.gconf.key);
    }

    return is_js_on;
}

// Set the state (enabled/disabled) of the JavaScript engine by using
// the active configuration backend (GConf/GSettings).
var set_js_state = function(js_state)
{
    if (ephy_cfg_backend.active == "gsettings")
    {
	// Gsettings
	var gsettings_client = ephy_cfg_backend.gsettings.instance;
	gsettings_client.set_boolean(ephy_cfg_backend.gsettings.key, js_state);
    }
    else
    {
	// GCOnf
	var gconf_client = ephy_cfg_backend.gconf.gconf_client;
	gconf_client.set_bool(ephy_cfg_backend.gconf.key, js_state);
    }
}

// Extension init

var detach_window = function(window)
{  
    windows.pop(window);
    
    // Must be the last window.  Remove the callback for GConf value
    // changes.
    if (windows.length == 0 && ephy_cfg_backend.gconf.gconf_notify_id)
    {
	var gconf_client = ephy_cfg_backend.gconf.gconf_client;
	gconf_client.notify_remove(ephy_cfg_backend.gconf.gconf_notify_id);
    }

    if (!ephy_no_statusbar)
    {
	destroy_statusbar_icon(window);
    }

    destroy_ephy_document_popup_entry(window);
}

var attach_window = function(window)
{
    detect_statusbar_capability(window);
    detect_cfg_backend();

    if (ephy_cfg_backend.active == "gconf")
    {

	if (!ephy_cfg_backend.gconf.gconf_client)
	{
	    ephy_cfg_backend.gconf.gconf_client = GConf.Client.get_default();
	}

	// If this is called too early and out of this function
	// Epiphany quits with segmentation fault. This way it is
	// still one instance for the entire extension/
	if (!ephy_cfg_backend.gconf.gconf_notify_id)
	{
	    var gconf_client = ephy_cfg_backend.gconf.gconf_client;
	    ephy_cfg_backend.gconf.gconf_notify_id =
		gconf_client.notify_add(ephy_cfg_backend.gconf.key,
					gconf_value_changed_cb);
	}
    }
    else
    {
	if (!ephy_cfg_backend.gsettings.instance)
	{
	    ephy_cfg_backend.gsettings.instance = new Gio.Settings({
		schema: ephy_cfg_backend.gsettings.schema
	    });

	    var gsettings = ephy_cfg_backend.gsettings.instance; 
	    gsettings.signal.changed.connect(gsettings_value_changed_cb);
	}
    }

    // Render an icon on the statusbar if it is present.
    if (!ephy_no_statusbar)
    {
	create_statusbar_icon(window);
    }

    create_ephy_document_popup_entry(window);
    windows.push(window);
}

extension=
{
    attach_window: attach_window,
    detach_window: detach_window,
}
